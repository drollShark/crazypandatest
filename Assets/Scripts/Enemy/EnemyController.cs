using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Animator))]
public class EnemyController : MonoBehaviour
{
    private Animator animator;

    public static Action onEnemyHit;

    private void Start()
    {
        animator = GetComponent<Animator>();
    }


    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.tag == "Player")
        {
            onEnemyHit?.Invoke();

            animator.SetBool("isHit", true);
        }
    }
}
