using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PaperPlaneMovement : MonoBehaviour
{
    [SerializeField]
    private float _speed;
    public float Speed => _speed >= 0 ? _speed : 1.5f;  

    [SerializeField]
    private float _rotateSpeed;
    public float RotateSpeed => _rotateSpeed > 0 ? _rotateSpeed : 60.0f;

    private float _horizontalInput;
    private float _verticalInput;

    public Joystick joystick;

    private bool isAlive = true;

    private void OnEnable()
    {
        Paper_Plane_Health.onPaperPlaneDaed += OnPaaperPlaneDaed;
    }

    private void OnDisable()
    {
        Paper_Plane_Health.onPaperPlaneDaed -= OnPaaperPlaneDaed;
    }

    void OnPaaperPlaneDaed()
    {
        isAlive = false;
    }

    private void Start()
    {
        _speed = Speed;
        _rotateSpeed = RotateSpeed;
    }

    void Update()
    {
        if (isAlive == true)
        {
            Rotation();
            Movement();
        }
    }

    void Movement()
    {
     
       transform.Translate(Vector3.forward * _speed * Time.deltaTime, Space.Self);
        
    }

    void Rotation()
    {
        _horizontalInput = joystick.Horizontal * _rotateSpeed * Time.deltaTime;
        _verticalInput = -joystick.Vertical * _rotateSpeed * Time.deltaTime;

        transform.Rotate(0, _horizontalInput, 0, Space.World);
        transform.Rotate(_verticalInput, 0, 0, Space.Self);

        
    }
}
