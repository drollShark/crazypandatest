using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class Paper_Plane_Health : MonoBehaviour
{
    private Rigidbody rb;

    public static Action onPaperPlaneDaed;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }

    private void OnCollisionEnter(Collision collision)
    {
        rb.useGravity = true;
        rb.drag = 10;
        onPaperPlaneDaed?.Invoke();
    }
}
