using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using Cinemachine;

public class GameMode : MonoBehaviour
{
    [SerializeField]
    private float _loadTime;
    public float LoadTime => _loadTime > 0 ? _loadTime : 5.0f;

    public CinemachineVirtualCamera virtualCamera;

    private GameObject LookAtPoint;

    private void OnEnable()
    {
        EnemyController.onEnemyHit += OnHitEnemy;
        Paper_Plane_Health.onPaperPlaneDaed += onPlayerDead;
    }

    private void OnDisable()
    {
        EnemyController.onEnemyHit -= OnHitEnemy;
        Paper_Plane_Health.onPaperPlaneDaed -= onPlayerDead;
    }

    private void Start()
    {
        _loadTime = LoadTime;

        LookAtPoint = GameObject.FindGameObjectWithTag("LookAtPoint");
    }

    void onPlayerDead()
    {
        Invoke("LosingSceneLoad", _loadTime/3);
    }

    void OnHitEnemy()
    {
        virtualCamera.LookAt = LookAtPoint.transform;
        Invoke("WinLoadScene", _loadTime);
    }

    void WinLoadScene()
    {
        SceneManager.LoadScene(0);

    }

    void LosingSceneLoad()
    {
        SceneManager.LoadScene(0);
    }
}
